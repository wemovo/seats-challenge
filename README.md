# Bus Seats

### Problem
A bus of our fleet has a capacity of **N** seats, **M** of these are booked and **P** are still available. 
**R** users are currently considering booking a seat online.
How can we avoid to overbook our bus? Explain your reasoning and solutions, step by step. 
Try to be as clear and concise as possible. Pseudo code in the answer is considered a plus.

### What we'll be evaluating
We want to understand your problem solving skills and your knowledge of common issues that may arise in our field of work.

###  Delivery
Once you're done send us your solutions via email or create a pull request.

###  Talk with us
We're more than happy to give you support and discuss any problem you have along the way. If you have questions don't hesitate in approaching us!